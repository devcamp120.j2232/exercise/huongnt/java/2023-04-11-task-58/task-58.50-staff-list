package com.devcamp.stafflistapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StafflistapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StafflistapiApplication.class, args);
	}

}
