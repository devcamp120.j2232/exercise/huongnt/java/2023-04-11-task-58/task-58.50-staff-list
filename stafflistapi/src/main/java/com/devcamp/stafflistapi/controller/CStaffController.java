package com.devcamp.stafflistapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.stafflistapi.model.CStaff;
import com.devcamp.stafflistapi.repository.IStaffRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CStaffController {
    @Autowired
    IStaffRepository pStaffRepository;
    @GetMapping("/staff")
    public ResponseEntity<List<CStaff>> getAllStaff(){
        try{
            List<CStaff> listStaff = new ArrayList<CStaff>();

            pStaffRepository.findAll()
            .forEach(listStaff::add);
            return new ResponseEntity<>(listStaff, HttpStatus.OK);

        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
    
}
