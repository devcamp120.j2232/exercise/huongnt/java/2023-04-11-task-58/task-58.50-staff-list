package com.devcamp.stafflistapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.stafflistapi.model.CStaff;

public interface IStaffRepository extends JpaRepository<CStaff, Long>{
    
}
